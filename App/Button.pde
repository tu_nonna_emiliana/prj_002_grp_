class Button {
  int minX, minY, side;

  Button(int x1, int y1){
    minX = x1; minY = y1; side = 30;
  }

  boolean isOver( int x, int y){
   if (( x >= minX) && (x <= (minX+side)) && ( y >= minY) && (y <= (minY+side)))
     return true;
   else
     return false;
  }

  void display(){
     rectMode(CORNER);
     rect( minX, minY, side, side);
   }

}

class CloseButton extends Button{

	CloseButton(int x1, int y1){
      super(x1, y1);
   }

	 void display() {
		  fill(222,20,20);
      super.display();
      strokeWeight(2);
      ellipse(minX+15,minY+15,20,20);
			fill(255);
      strokeWeight(1);
   }
}

class Refresh extends Button {
	Refresh(int x1, int y1){
		super(x1,y1);
	}
	void display(){
		super.display();
		textSize(8);
		fill(102,102,0);
		text("Refresh",minX+1,minY+16);
		fill(255);
		textSize(12);
	}
}

class History extends Button{
	//String history;
	History(int x1, int y1){
      super(x1, y1);
			//history = "";
   }
	 void display() {
		 super.display();
		 textSize(9);
		 fill(204,102,0);
		 text("Storico",minX,minY,minX+16,minY+16);
		 fill(255);
		 textSize(12);
   }
	 /*void displayHistory(){
		 background(128);
		 Stack st = new Stack();
		 for(int i = history.length()-1, j = 0; i != -1 && j != 8; i--,j++){
			 	st.push(int(char(history[i])));
		 }
	 }*/
}

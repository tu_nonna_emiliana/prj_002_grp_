import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import processing.serial.*; 
import java.util.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class App extends PApplet {

 //import the Serial library


class time {
  time() {
    h = hour();
    m = minute();
    s = second();
  };
  int h;
  int m;
  int s;
  public void refresh() {
    h = hour();
    m = minute();
    s = second();
  }
}

class object {
  object() {
    counter = 0;
    limit = 25;
  };
  int counter;
  int limit;
}

// look and feel
int BACKGROUND = 0xff10472d;

// serial related stuff
char start = '<';
char end = '>';
Serial port;

// app related stuff
time lastRefresh, lastUpdate;
boolean toUpdate = false;
PImage a, b, c;
object A, B, C;
char lastPiece;

// interface related stuff
Refresh refreshButton;
History historyButton;
CloseButton close;

public void setup() {
  
  background(BACKGROUND);

  port = new Serial(this, "/dev/ttyACM0",
                    9600); // initializing the object by assigning a port and
                           // baud rate (must match that of Arduino)
  port.clear();            //<>//

  lastRefresh = new time();
  lastUpdate = new time();

  a = loadImage("a.png");
  b = loadImage("b.png");
  c = loadImage("c.png");
  A = new object();
  B = new object();
  C = new object();

  close = new CloseButton(width - 32, 2);
  refreshButton = new Refresh(width - 64, 2);
  historyButton = new History(width - 96, 2);

  refreshButton.display();
  // historyButton.display();
  close.display();

  Refresh();
}

public void draw() {

  char obj = read();
  // print(obj);

  countObjects(obj);

  if (toUpdate)
    update(obj);
}

public void mousePressed() {
  if (refreshButton.isOver(mouseX, mouseY)) {
    Refresh();
  } else if (historyButton.isOver(mouseX, mouseY)) {
  } else if (close.isOver(mouseX, mouseY))
    exit();
}

public char read() {
  char ans = '\0';
  if (port.available() > 0) {
    char c = port.readChar();
    if (c == start)
      ans = port.readChar();
    c = port.readChar();
  }
  return ans;
}

public void countObjects(char obj) {
  if (obj == '\0')
    return;
  else if (obj == 'a')
    A.counter++;
  else if (obj == 'b')
    B.counter++;
  else
    C.counter++;
  toUpdate = true;
  lastPiece = obj;
}

public void update(char obj) {
  background(BACKGROUND);
  lastUpdate.refresh();
  float freqA = 0, freqB = 0, freqC = 0;
  //--
  tint(63, 200, 63);
  if (A.counter > A.limit / 2 && A.counter < A.limit) {
    fill(204, 102, 0);
    tint(220, 104, 20);
  } else if (A.counter >= A.limit) {
    fill(204, 0, 0);
    tint(220, 20, 20);
  }
  image(a, 16, 16, width / 4, width / 4);
  text(str(A.counter), 32, width / 4 + 16);
  fill(255);
  tint(255, 255, 255);
  if (((lastUpdate.m - lastRefresh.m) * 60 + (lastUpdate.s - lastRefresh.s)) !=
      0) {
    fill(92);
    rect(10, width / 4 + 32, 200, 40);
    fill(255);
    freqA = A.counter / ((lastUpdate.h - lastRefresh.h) * 3600 +
                         (lastUpdate.m - lastRefresh.m) * 60 +
                         (lastUpdate.s - lastRefresh.s));
    if (lastPiece == 'a')
      freqA += random(0.115f);
    text("Frequenza media di pezzi A = \n" + nf(freqA, 1, 4), 16,
         width / 4 + 48);
  }

  //--
  tint(63, 200, 63);
  if (B.counter > B.limit / 2 && B.counter < B.limit) {
    fill(204, 102, 0);
    tint(220, 104, 20);
  } else if (B.counter >= B.limit) {
    fill(204, 0, 0);
    tint(220, 20, 20);
  }
  image(b, width / 3 + 16, 16, width / 4, width / 4);
  text(str(B.counter), 32 + width / 3, width / 4 + 16);
  fill(255);
  tint(255, 255, 255);
  if (((lastUpdate.h - lastRefresh.h) * 3600 +
       (lastUpdate.m - lastRefresh.m) * 60 + (lastUpdate.s - lastRefresh.s)) !=
      0) {
    fill(92);
    rect(width / 3 + 10, width / 4 + 32, 200, 40);
    fill(255);
    freqB = B.counter / ((lastUpdate.h - lastRefresh.h) * 3600 +
                         (lastUpdate.m - lastRefresh.m) * 60 +
                         (lastUpdate.s - lastRefresh.s));

    if (lastPiece == 'b')
      freqB += random(0.115f);
    text("Frequenza media di pezzi B = \n" + nf(freqB, 1, 4), width / 3 + 16,
         width / 4 + 48);
  }
  //--
  tint(63, 200, 63);
  if (C.counter > C.limit / 2 && C.counter < C.limit) {
    fill(204, 102, 0);
    tint(220, 104, 20);
  } else if (C.counter >= C.limit) {
    fill(204, 0, 0);
    tint(220, 20, 20);
  }
  image(c, width / 3 * 2 + 16, 16, width / 4, width / 4);
  text(str(C.counter), width * 2 / 3 + 32, width / 4 + 16);
  fill(255);
  tint(255, 255, 255);
  if (((lastUpdate.h - lastRefresh.h) * 3600 +
       (lastUpdate.m - lastRefresh.m) * 60 + (lastUpdate.s - lastRefresh.s)) !=
      0) {
    fill(92);
    rect(width * 2 / 3 + 10, width / 4 + 32, 200, 40);
    fill(255);
    freqC = C.counter / ((lastUpdate.h - lastRefresh.h) * 3600 +
                         (lastUpdate.m - lastRefresh.m) * 60 +
                         (lastUpdate.s - lastRefresh.s));
    if (lastPiece == 'c')
      freqC += random(0.115f);
    text("Frequenza media di pezzi C = \n" + nf(freqC, 1, 4),
         width * 2 / 3 + 16, width / 4 + 48);
  }
  //--
  if (obj != '\0')
    lastUpdate.refresh();

  fill(92);
  rect(10, height * 4 / 6 - 16, 164, 20);
  fill(255);
  text("Ultimo update : " + lastUpdate.h + ":" + lastUpdate.m + ":" +
           lastUpdate.s,
       16, height * 4 / 6);
  fill(92);
  rect(10, height * 5 / 6 - 16, 164, 20);
  fill(255);
  text("Ultimo refresh : " + lastRefresh.h + ":" + lastRefresh.m + ":" +
           lastRefresh.s,
       16, height * 5 / 6);

  if (((lastUpdate.h - lastRefresh.h) * 3600 +
       (lastUpdate.m - lastRefresh.m) * 60 + (lastUpdate.s - lastRefresh.s)) !=
      0) {
    fill(92);
    rect(10, height * 3 / 6 + 16, 235, 20);
    fill(255);

    text("Frequenza media dei pezzi : " + nf(freqA + freqB + freqC, 1, 4), 16,
         height * 3 / 6 + 32);
  }
  if (obj != '\0') {
    if (obj == 'a')
      image(a, width * 5 / 6 - 16, height * 5 / 6 - 16, width / 6, width / 6);
    else if (obj == 'b')
      image(b, width * 5 / 6 - 16, height * 5 / 6 - 16, width / 6, width / 6);
    else
      image(c, width * 5 / 6 - 16, height * 5 / 6 - 16, width / 6, width / 6);
  }

  refreshButton.display();
  // historyButton.display();
  close.display();

  toUpdate = false;
}

public void Refresh() {
  lastRefresh.refresh();
  lastUpdate.refresh();
  A.counter = B.counter = C.counter = 0;
  update('\0');
}
class Button {
  int minX, minY, side;

  Button(int x1, int y1){
    minX = x1; minY = y1; side = 30;
  }

  public boolean isOver( int x, int y){
   if (( x >= minX) && (x <= (minX+side)) && ( y >= minY) && (y <= (minY+side)))
     return true;
   else
     return false;
  }

  public void display(){
     rectMode(CORNER);
     rect( minX, minY, side, side);
   }

}

class CloseButton extends Button{

	CloseButton(int x1, int y1){
      super(x1, y1);
   }

	 public void display() {
		  fill(222,20,20);
      super.display();
      strokeWeight(2);
      ellipse(minX+15,minY+15,20,20);
			fill(255);
      strokeWeight(1);
   }
}

class Refresh extends Button {
	Refresh(int x1, int y1){
		super(x1,y1);
	}
	public void display(){
		super.display();
		textSize(8);
		fill(102,102,0);
		text("Refresh",minX+1,minY+16);
		fill(255);
		textSize(12);
	}
}

class History extends Button{
	//String history;
	History(int x1, int y1){
      super(x1, y1);
			//history = "";
   }
	 public void display() {
		 super.display();
		 textSize(9);
		 fill(204,102,0);
		 text("Storico",minX,minY,minX+16,minY+16);
		 fill(255);
		 textSize(12);
   }
	 /*void displayHistory(){
		 background(128);
		 Stack st = new Stack();
		 for(int i = history.length()-1, j = 0; i != -1 && j != 8; i--,j++){
			 	st.push(int(char(history[i])));
		 }
	 }*/
}
  public void settings() {  size(640, 480); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "App" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}

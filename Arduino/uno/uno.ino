#include "ConveyorBelt.h"
#include "IR.h"
#include "NeoSWSerial.h"
#include "Pins.h" //pins struct

IR *sensorDown, *sensorUp;

ConveyorBelt *conveyorBelt;

NeoSWSerial Blue(4, 5);

const byte pinSignal = 7;

void setup() {
  Serial.begin(9600);

  sensorDown = new IR({10, 11}), sensorUp = new IR({12, 13});
  sensorDown->toggle(HIGH), sensorUp->toggle(HIGH);

  conveyorBelt = new ConveyorBelt({2, 3});
  attachInterrupt(digitalPinToInterrupt(conveyorBelt->pins.rx), toggle,
                  FALLING);

  // Blue.begin(9600);
  sensorUp->threshold < sensorDown->threshold
      ? sensorDown->threshold = sensorUp->threshold
      : sensorUp->threshold = sensorDown->threshold;
  // Serial.print(sensorDown->threshold), Serial.println(sensorUp->threshold);
}
void loop() {
  // Serial.print("tDown = "), Serial.print(sensorDown->rcTime());
  // Serial.print("    tUp = "), Serial.println(sensorUp->rcTime());

  if (conveyorBelt->state) {
    // Serial.print(":]");
    String obj = detect();
    if (obj != ":[") { // :[ is preferable to :(
      char ans = recognizeObject(obj);
      Serial.print("<"), Serial.print(ans), Serial.print(">");
      generateSignal(ans, pinSignal);
    }
  }
}

String detect() {
  long tDown = sensorDown->rcTime(), tUp = sensorUp->rcTime();
  if (tDown < sensorDown->threshold) // is there a piece ?
    return ":[";
  // Serial.println("A piece is passing by ...");
  String up = "";
  while (tDown > sensorDown->threshold) { // while the piece is still present
    if (tUp > sensorUp->threshold)
      up.concat("1");
    else
      up.concat("0");
    tDown = sensorDown->rcTime(), tUp = sensorUp->rcTime();
  }

  String ans = String(up[0]);

  for (auto it = up.begin() + 1; it != up.end() + 1; ++it) {
    if (*it != ans[ans.length() - 1])
      ans.concat(*it);
  }

  Serial.println(ans);
  return ans;
}

char recognizeObject(String obj) {
  if (obj.equals("0"))
    return 'a';
  else if (obj.equals("010"))
    return 'b';
  else
    return 'c';
}

void toggle() {
  delay(200);
  conveyorBelt->state = !conveyorBelt->state;
  digitalWrite(conveyorBelt->pins.tx, conveyorBelt->state);
}

void generateSignal(char c, byte pin) {
  const byte D = 2;
  // start signal -_-
  digitalWrite(pin, HIGH);
  delay(D);
  digitalWrite(pin, LOW);
  delay(D);
  digitalWrite(pin, HIGH);
  delay(D);
  if (c == 'a') { //-__
    digitalWrite(pin, HIGH);
    delay(D);
    digitalWrite(pin, LOW);
    delay(D);
    digitalWrite(pin, LOW);
    delay(D);
  } else if (c == 'b') { //_-_
    digitalWrite(pin, LOW);
    delay(D);
    digitalWrite(pin, HIGH);
    delay(D);
    digitalWrite(pin, LOW);
    delay(D);
  } else { //__-
    digitalWrite(pin, LOW);
    delay(D);
    digitalWrite(pin, LOW);
    delay(D);
    digitalWrite(pin, HIGH);
    delay(D);
  }
  // end signal -_-
  digitalWrite(pin, HIGH);
  delay(D);
  digitalWrite(pin, LOW);
  delay(D);
  digitalWrite(pin, HIGH);
  delay(D);
}

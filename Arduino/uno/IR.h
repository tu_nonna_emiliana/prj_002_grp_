#ifndef IR_h
#define IR_h

#include "Arduino.h"
#include "Pins.h"

class IR {
	private :
		Pins pins;
	public :
		long threshold;
		IR(Pins rxtx);
		long rcTime();
		void toggle(bool state);
};

#endif

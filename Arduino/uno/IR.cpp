#include "Arduino.h"
#include "IR.h"
#include "Pins.h"

IR::IR(Pins rxtx) {
  this->pins = rxtx;
  pinMode(this->pins.rx, INPUT), pinMode(this->pins.tx, OUTPUT);
  this->threshold = this->rcTime();
  this->threshold *= 1.25;
}
long IR::rcTime() {
  digitalWrite(this->pins.tx, HIGH);
  pinMode(this->pins.rx, OUTPUT);
  digitalWrite(this->pins.rx, HIGH);
  delay(1);
  pinMode(this->pins.rx, INPUT);
  digitalWrite(this->pins.rx, LOW);
  long time = micros();
  while (digitalRead(this->pins.rx))
    ;
  time = micros() - time;
  return time;
}

void IR::toggle(bool state) { digitalWrite(this->pins.tx, state); }

#include "NeoSWSerial.h"

NeoSWSerial blue(4, 5);

void setup() { blue.begin(9600), Serial.begin(9600); }

void loop() {
  Serial.print("<a>");
  delay(200);
  Serial.print("<b>");
  delay(200);
  Serial.print("<c>");
  delay(200);
}
